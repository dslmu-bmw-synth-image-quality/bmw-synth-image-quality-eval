from notebooks.report import *


def file_cleaner(folder_path, data_folder="data"):
    """
    If files are saved with complete system paths, this function truncates them to the expected style.
    - folder_path: folder with the score files to clear up
    - data_folder: name of the folder in the images where saved when computation was run
    """
    files = [file.name for file in os.scandir(folder_path) if file.is_file() and file.name[-4:] == ".txt"]

    for file in files:
        imgs, vals = [], []
        score_path = os.path.join(folder_path, file)
        with open(score_path) as f:
            for f_path, val in chunker(f.readlines(), 2):
                f_path = f_path[9:-5]
                f_path = f_path.split(data_folder)[1][1:]
                imgs.append(f_path)
                vals.append(float(val))

        with open(score_path, 'wt') as f:
            for image_i in range(0, len(imgs)):
                this_score = str(float(vals[image_i]))
                image_file = str(imgs[image_i])
                f.write("score of " + image_file + " is:\n")
                f.write(this_score)
                f.write("\n")
