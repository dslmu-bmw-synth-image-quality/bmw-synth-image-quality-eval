import os
import pathlib
import pickle

from itertools import compress
from sklearn import mixture
from tqdm import tqdm

from models import gmm_score, write_act


def path_list(path, filter=None, n_values=0, last=False):
    """
    returns names of all subdirectories of path. Defaults to last subdirectory.
    - path: path in which to look for subdirectories
    - filter: list of booleans of which images to include (takes precedence over other inputs)
    - n_values: returns n values. First n if last=False, otherwise last n values

    """
    directories = [dir.name for dir in os.scandir(path) if dir.is_dir()]
    directories.sort()

    if n_values > len(directories):
        n_values = len(directories)

    if len(directories) == 0:
        return []

    if filter is None:
        if n_values and not last:
            return directories[:n_values]
        elif n_values and last:
            return directories[-n_values:]
        else:
            return directories[-1]
    else:
        return list(compress(directories, filter))


def compute_scores(data_path, output_file, pca_path, gmm_path, data_folder, filter=None, n_values=0, last=False,
                   silent=True,
                   batch_size=20, cuda=False, dims=2048, bmw_inception=False, custom_ae=False, ckpt_path=None,
                   file_list=False):
    """
    Computes the scores for all images in the subdirectories of path and saves them to the specified .txt file.
    All folders must already be created.
    - data_path: path in which to look for subdirectories. If there are no subdirectories the path is passed through
    - result_path: if .txt filepath is passed, results are saved there. With only a directory the name is auto-generated out of the filter (recommended)
    - data_folder: folder in which all images are stored. Required to make saved paths system independent and passed through to get_activations.
    - filter: list of booleans of which subfolders to include (takes precedence over other inputs)
    - n_values: returns n values. First n if last=False, otherwise last n values
    - silent: suppresses statement of which folders are computed
    """
    if not file_list:
        directories = path_list(data_path, filter, n_values, last)

        if len(directories) == 0:
            paths = [data_path]
        else:
            paths = [os.path.join(data_path, dir) for dir in directories]

        if not silent:
            print("The following directories are used to compute scores:\n", directories)

        model = data_path.split("_")[-1]
        if len(model) > 5:
            model = data_path.split("\\")[-1]
            model = model.split("/")[-1]

        if output_file[-4:] != ".txt":
            output_name = f"{model}__" + "_".join([str(dir[:6]) for dir in directories]) + ".txt"
            output_file = os.path.join(output_file, output_name)

        print(f"Computing scores for {model}:")
    else:
        paths = data_path

    _ = gmm_score.calculate_fid_given_several_paths(paths, batch_size=batch_size, cuda=cuda, dims=dims,
                                                    pca_path=pca_path, gmm_path=gmm_path, data_folder=data_folder,
                                                    output_file=output_file,
                                                    bmw_inception=bmw_inception, custom_ae=custom_ae,
                                                    ckpt_path=ckpt_path, file_list=file_list)


def create_gmm_model(dataset_path, act_path, pca_path, gmm_path, bmw_inception=False, batch_size=20, gpu=True,
                     pca_rate=0.95, dims=2048, kernel_number=70, custom_ae=False, ckpt_path=None, file_list=False):
    """
        Creates the GMM based on the data passed to it from dataset_path and pickles:
        - path to save the activation for the images: act_path
        - path to save the pca results: pca_path
        - path to save the gmm model: gmm_path
    """

    write_act.calculate_fid_given_paths(dataset_path, batch_size, gpu, dims, pca_path, pca_rate, act_path,
                                        bmw_inception, custom_ae=custom_ae, ckpt_path=ckpt_path, file_list=file_list)

    act = pickle.load(open(act_path, "rb"))
    gact = mixture.GaussianMixture(n_components=kernel_number, covariance_type='full')
    gact.fit(act)
    pickle.dump(gact, open(gmm_path, "wb+"))
