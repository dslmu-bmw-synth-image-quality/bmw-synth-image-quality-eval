#!/usr/bin/env python3
import os
import pathlib
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

import numpy as np
import torch
from scipy import linalg
# from scipy.misc import imread
from PIL import Image
from torch.nn.functional import adaptive_avg_pool2d

import pickle
from scipy.stats import multivariate_normal
from sklearn import mixture

try:
    from tqdm import tqdm
except ImportError:
    # If not tqdm is not available, provide a mock version of it
    def tqdm(x):
        return x

from .inception import InceptionV3
from models.autoencoder import AutoEncoder
from models.unet.unet import UNet, UNetEncoder
from models.unet.unet_reduced import UNetReduced, UNetReducedEncoder

parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
parser.add_argument('path', type=str, nargs=1,
                    help=('Path to the generated images or '
                          'to .npz statistic files'))
parser.add_argument('--batch-size', type=int, default=50,
                    help='Batch size to use')
parser.add_argument('--dims', type=int, default=2048,
                    choices=list(InceptionV3.BLOCK_INDEX_BY_DIM),
                    help=('Dimensionality of Inception features to use. '
                          'By default, uses pool3 features'))
parser.add_argument('-c', '--gpu', default='', type=str,
                    help='GPU to use (leave blank for CPU only)')
parser.add_argument('--pca_path', type=str, default=None)
# "/mnt/blob/code/image-judge/gaussian/pca_stat/pca_all_95.pkl"
parser.add_argument('--gmm_path', type=str, default="/mnt/blob/code/image-judge/gaussian/pca_stat/stat_cat/act95_7")
parser.add_argument('--output_file', type=str,
                    default="/mnt/blob/datasets/generation_results/score_results/try_out.txt")


def imread(filename):
    return np.asarray(Image.open(filename).convert('RGB'), dtype=np.uint8)[..., :3]


def get_activations(files, model, batch_size, dims, cuda, verbose, pca_path, gmm_path, output_file, data_folder):
    """
    Processes the images using the precomputed pca & gmm of the real image distribution, computing their scores and
    writing the scores per image to the specified place (line 109 following). These are written relative to the specified
    data_folder, such that results can be easily transferred between different systems and no file path issues arise.
    """
    model.eval()

    if len(files) % batch_size != 0:
        print(('Warning: number of images is not a multiple of the '
               'batch size. Some samples are going to be ignored.'))
    if batch_size > len(files):
        print(('Warning: batch size is bigger than the data size. '
               'Setting batch size to data size'))
        batch_size = len(files)

    n_batches = len(files) // batch_size
    n_used_imgs = n_batches * batch_size

    pred_arr = np.empty((n_used_imgs, dims))

    pca_gmm_path = gmm_path
    pca_gmm = pickle.load(open(gmm_path, "rb"))
    file_path = output_file

    if pca_path != None:
        pca = pickle.load(open(pca_path, "rb"))

    score_list = []

    with open(file_path, 'wt') as f:
        for i in tqdm(range(n_batches)):
            start = i * batch_size
            end = start + batch_size
            images = np.array([imread(str(f)).astype(np.float32) for f in files[start:end]])

            # print(images.shape)
            # Reshape to (n_images, 3, height, width)
            images = images.transpose((0, 3, 1, 2))
            images /= 255

            batch = torch.from_numpy(images).type(torch.FloatTensor)
            if cuda:
                batch = batch.cuda()

            if isinstance(model, UNetEncoder) or isinstance(model, UNetReducedEncoder):
                pred = model(batch)[-1]
            else:
                pred = model(batch)[0]

            if pca_path != None:
                pred = pred.detach().cpu().numpy()
                pred = pca.transform(pred[:, :, 0, 0])
                prop = pca_gmm.score_samples(pred)
            else:
                print(pred[:, :, 0, 0].cpu().numpy().shape)
                prop, _ = pca_gmm.score_samples(pred[:, :, 0, 0].cpu().numpy())

            for image_i in range(0, batch_size):
                this_score = str(float(prop[image_i]))
                image_file = str(files[start + image_i])
                # splits the given path at the data folder and so saves everything from the data folder on
                image_file = image_file.split(os.path.split(os.path.basename(data_folder))[1])[1][1:]
                # new_name = str(int(prop[image_i]))+"_"+image_file
                f.write("score of " + image_file + " is:\n")
                f.write(this_score)
                # f.write(new_name)
                f.write("\n")

    return pred_arr


def calculate_activation_statistics(files, model, batch_size, dims, cuda, pca_path, gmm_path, output_file, data_folder):
    """
    Computes overall activations statistics and calls get_activations
    """
    verbose = False
    act = get_activations(files, model, batch_size, dims, cuda, verbose, pca_path, gmm_path, output_file, data_folder)
    mu = np.mean(act, axis=0)
    sigma = np.cov(act, rowvar=False)
    return mu, sigma


def _compute_statistics_of_path(path, model, batch_size, dims, cuda, pca_path, gmm_path, output_file):
    if path.endswith('.npz'):
        f = np.load(path)
        m, s = f['mu'][:], f['sigma'][:]
        f.close()

    else:
        path = pathlib.Path(path)

        # image_file = open(path)
        # files = image_file.readlines()

        files = list(path.glob('*.jpg')) + list(path.glob('*.jpeg')) + list(path.glob('*.png'))
        m, s = calculate_activation_statistics(files, model, batch_size, dims, cuda, pca_path, gmm_path, output_file)

    return m, s


def calculate_fid_given_paths(paths, batch_size, cuda, dims, pca_path, gmm_path, output_file, bmw_inception=False):
    for p in paths:
        if not os.path.exists(p):
            raise RuntimeError('Invalid path: %s' % p)

    block_idx = InceptionV3.BLOCK_INDEX_BY_DIM[dims]

    model = InceptionV3([block_idx], use_bmw_inception=bmw_inception)
    if cuda:
        model.cuda()

    m1, s1 = _compute_statistics_of_path(paths[0], model, batch_size, dims, cuda, pca_path, gmm_path, output_file)

    return 777


def calculate_fid_given_images(files, batch_size, cuda, dims, pca_path, gmm_path, output_file):
    block_idx = InceptionV3.BLOCK_INDEX_BY_DIM[dims]

    model = InceptionV3([block_idx])
    if cuda:
        model.cuda()
    calculate_activation_statistics(files, model, batch_size, dims, cuda, pca_path, gmm_path, output_file)


def _compute_statistics_of_several_paths(paths, model, batch_size, dims, cuda, pca_path, gmm_path, output_file,
                                         data_folder, file_list=False):
    """
    Expects several paths in a list and calls the function for computing their statistics and saves them to a single file.
    - file_list: If true, expects a list of images for paths and processes them directly
    """
    if (type(paths) is not list) and paths.endswith('.npz'):
        f = np.load(paths)
        m, s = f['mu'][:], f['sigma'][:]
        f.close()

    else:
        if file_list:
            files = paths
        else:
            files = []
            for path in paths:
                path = pathlib.Path(path)

                files += list(path.glob('*.jpg')) + list(path.glob('*.jpeg')) + list(path.glob('*.png'))

        m, s = calculate_activation_statistics(files, model, batch_size, dims, cuda, pca_path, gmm_path, output_file,
                                               data_folder)

    return m, s


def calculate_fid_given_several_paths(paths, batch_size, cuda, dims, pca_path, gmm_path, output_file, data_folder,
                                      bmw_inception=False, custom_ae=False, ckpt_path=None, file_list=False):
    """
        Loads the required model and calls the computation function.
    """
    if not file_list:
        for p in paths:
            if not os.path.exists(p):
                raise RuntimeError('Invalid path: %s' % p)

    if custom_ae and isinstance(ckpt_path, str):
        if 'reduced' in ckpt_path:
            model = AutoEncoder.load_from_checkpoint(ckpt_path,
                                                     model=UNetReduced(n_channels=3, bilinear=False),
                                                     train_set_dataloader=None,
                                                     val_set_dataloader=None)
        else:
            model = AutoEncoder.load_from_checkpoint(ckpt_path,
                                                     model=UNet(n_channels=3, bilinear=False),
                                                     train_set_dataloader=None,
                                                     val_set_dataloader=None)
        model.eval()
        model = model.get_encoder()
    else:
        block_idx = InceptionV3.BLOCK_INDEX_BY_DIM[dims]
        model = InceptionV3([block_idx], use_bmw_inception=bmw_inception)

    if cuda:
        model.cuda()

    m1, s1 = _compute_statistics_of_several_paths(paths, model, batch_size, dims, cuda, pca_path, gmm_path, output_file,
                                                  data_folder=data_folder, file_list=file_list)

    return 777


if __name__ == '__main__':
    args = parser.parse_args()
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu

    fid_value = calculate_fid_given_paths(args.path, args.batch_size, args.gpu != '', args.dims, args.pca_path,
                                          args.gmm_path, args.output_file)
    print('FID: ', fid_value)
