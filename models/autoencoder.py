import torch
import torch.nn as nn
import pytorch_lightning as pl


class AutoEncoder(pl.LightningModule):
    def __init__(self, model, train_set_dataloader, val_set_dataloader):
        super().__init__()

        self.model = model
        self.train_set_dataloader = train_set_dataloader
        self.val_set_dataloader = val_set_dataloader
        self.loss = nn.MSELoss()

    def forward(self, x):
        reconstruction = self.model(x)

        return reconstruction

    def configure_optimizers(self):
        model_params = list(self.model.parameters())

        optim = torch.optim.Adam(
            model_params,
        )

        return optim

    def general_step(self, images, batch_idx, mode):
        reconstruction = self.forward(images)
        loss = self.loss(reconstruction, images)

        return loss, reconstruction

    def general_end(self, outputs, mode):
        # Average over all batches aggregated during one epoch
        avg_loss = torch.stack([x[mode + '_loss'] for x in outputs]).mean()

        return avg_loss

    def training_step(self, batch, batch_idx):
        loss, _ = self.general_step(batch, batch_idx, "train")

        # Generate and log the computation graph only at the first epoch
        if self.current_epoch == 0:
            self.logger.experiment.add_graph(AutoEncoder(model=self.model,
                                                         train_set_dataloader=self.train_set_dataloader,
                                                         val_set_dataloader=self.val_set_dataloader),
                                             torch.rand((1, 3, 128, 128)))

        self.logger.experiment.add_scalar("Loss/Train", loss, self.current_epoch)
        self.log("loss", loss)

        return {'loss': loss}

    def train_dataloader(self):
        return self.train_set_dataloader

    def val_dataloader(self):
        return self.val_set_dataloader

    def validation_step(self, batch, batch_idx):
        loss, _ = self.general_step(batch, batch_idx, "val")

        self.logger.experiment.add_scalar("Loss/Val", loss, self.current_epoch)
        self.log("val_loss", loss)

        return {'val_loss': loss}

    def get_encoder(self):
        return self.model.get_encoder()
