""" UNet: Convolutional Networks for Biomedical Image Segmentation
    Paper: https://arxiv.org/abs/1505.04597
"""

from models.unet.unet_modules import *


class UNetReducedEncoder(nn.Module):
    def __init__(self, n_channels, bilinear=False):
        super(UNetReducedEncoder, self).__init__()

        self.n_channels = n_channels
        self.bilinear = bilinear

        self.inc = DoubleConv(n_channels, 64)
        self.down1 = Down(64, 128)
        self.down2 = Down(128, 256)
        self.down3 = Down(256, 512)
        factor = 2 if bilinear else 1
        self.down4 = Down(512, 1024 // factor)
        self.down5 = Down(1024 // factor, 2048 //factor)

    def forward(self, x):
        enc_x = []

        enc_x.append(self.inc(x))
        enc_x.append(self.down1(enc_x[0]))
        enc_x.append(self.down2(enc_x[1]))
        enc_x.append(self.down3(enc_x[2]))
        enc_x.append(self.down4(enc_x[3]))
        enc_x.append(self.down5(enc_x[4]))

        return enc_x


class UNetReducedDecoder(nn.Module):
    def __init__(self, n_channels, bilinear=False):
        super(UNetReducedDecoder, self).__init__()

        self.n_channels = n_channels
        self.bilinear = bilinear

        factor = 2 if bilinear else 1
        self.up0 = Up(2048 // factor, 1024 // factor, bilinear)
        self.up1 = Up(1024, 512 // factor, bilinear)
        self.up2 = Up(512, 256 // factor, bilinear)
        self.up3 = Up(256, 128 // factor, bilinear)
        self.up4 = Up(128, 64, bilinear)
        self.outc = OutConv(64, n_channels)

    def forward(self, enc_x):
        x = self.up0(enc_x[5], enc_x[4])
        x = self.up1(x, enc_x[3])
        x = self.up2(x, enc_x[2])
        x = self.up3(x, enc_x[1])
        x = self.up4(x, enc_x[0])
        x = self.outc(x)

        return x


class UNetReduced(nn.Module):
    def __init__(self, n_channels, bilinear=False):
        super(UNetReduced, self).__init__()
        self.n_channels = n_channels
        self.bilinear = bilinear

        self.encoder = UNetReducedEncoder(n_channels=n_channels, bilinear=bilinear)
        self.decoder = UNetReducedDecoder(n_channels=n_channels, bilinear=bilinear)

    def forward(self, x):
        x = self.encoder(x)
        reconstruction = self.decoder(x)

        return reconstruction

    def get_encoder(self):
        return self.encoder
