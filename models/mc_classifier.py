import torch
import torch.nn as nn
import pytorch_lightning as pl


class MultiClassClassifier(pl.LightningModule):
    def __init__(self, params, inscri_to_class_map, encoder, train_set_dataloader, val_set_dataloader):
        super().__init__()

        self.params = params
        self.inscri_to_class_map = inscri_to_class_map
        self.encoder = encoder
        self.train_set_dataloader = train_set_dataloader
        self.val_set_dataloader = val_set_dataloader

        self.classifier = nn.Sequential(
            nn.Conv2d(in_channels=params['in_channels'],
                      out_channels=params['1x1conv_layers'][0],
                      kernel_size=1),
            nn.BatchNorm2d(params['1x1conv_layers'][0]),
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=params['1x1conv_layers'][0],
                      out_channels=params['1x1conv_layers'][1],
                      kernel_size=1),
            nn.BatchNorm2d(params['1x1conv_layers'][1]),
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels=params['1x1conv_layers'][1],
                      out_channels=params['num_classes'],
                      kernel_size=1),
            nn.BatchNorm2d(params['num_classes']),
            nn.ReLU(inplace=True),
            nn.AdaptiveMaxPool2d(output_size=1),
            nn.ReLU(inplace=True)
        )
        self.loss = nn.CrossEntropyLoss()

    def forward(self, batch):
        features = self.encoder(batch)
        pred = self.classifier(features[-1])    # Select only the embedding

        return pred

    def configure_optimizers(self):
        model_params = list(self.classifier.parameters())

        optim = torch.optim.Adam(
            model_params,
        )

        return optim

    def general_step(self, batch, batch_idx, mode):
        images = batch['image']
        pred_label_vector = self.forward(images)
        pred_label_vector = torch.squeeze(pred_label_vector)

        labels = batch['label']
        true_label_vector = torch.zeros(len(labels), len(self.inscri_to_class_map))
        for i in range(len(labels)):
            true_label_vector[i][batch['label'][i]] = 1

        loss = self.loss(pred_label_vector, true_label_vector)

        return loss, pred_label_vector, true_label_vector

    def general_end(self, outputs, mode):
        # Average over all batches aggregated during one epoch
        avg_loss = torch.stack([x[mode + '_loss'] for x in outputs]).mean()

        return avg_loss

    def training_step(self, batch, batch_idx):
        loss, _, _ = self.general_step(batch, batch_idx, "train")

        self.logger.experiment.add_scalar("Loss/Train", loss, self.current_epoch)
        self.log("loss", loss)

        return {'loss': loss}

    def train_dataloader(self):
        return self.train_set_dataloader

    def val_dataloader(self):
        return self.val_set_dataloader

    def validation_step(self, batch, batch_idx):
        loss, pred_labels, true_labels = self.general_step(batch, batch_idx, "val")

        y_pred_tensor = torch.argmax(pred_labels, dim=1)
        y_true_tensor = torch.argmax(true_labels, dim=1)

        count = torch.sum(y_pred_tensor == y_true_tensor)

        count_dict = {}

        for inscri_key, class_lab in self.inscri_to_class_map.items():
            count_dict[class_lab] = {'correct': 0,
                                     'total': 0}

        for pred, true in zip(y_pred_tensor.numpy(), y_true_tensor.numpy()):
            if pred == true:
                count_dict[true]['correct'] += 1
            count_dict[true]['total'] += 1

        accuracy = (count / len(true_labels)) * 100.0

        self.logger.experiment.add_scalar("Loss/Val", loss, self.current_epoch)
        self.logger.experiment.add_scalar("Accuracy/Val", accuracy, self.current_epoch)
        self.log("val_loss", loss)

        result_dict = {'val_loss': loss, 'accuracy': accuracy}

        for inscri_key, class_lab in self.inscri_to_class_map.items():
            class_acc = (count_dict[class_lab]['correct'] / count_dict[class_lab]['total']) * 100.0
            result_dict[inscri_key + '_accuracy'] = class_acc
            self.logger.experiment.add_scalar(inscri_key + "_accuracy" + "/Val", class_acc, self.current_epoch)

        return result_dict

    def get_encoder(self):
        return self.model.get_encoder()

    def test_step(self, batch, batch_idx):
        return self.validation_step(batch, batch_idx)

