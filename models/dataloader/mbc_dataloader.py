import os

from torchvision.io import read_image
from torch.utils.data import Dataset
import numpy as np
import itertools
import torch

class ClassificationDatasetMBC(Dataset):
    '''
    Dataset Class for MBC-GIQA Classification of Generated Images
    '''

    def __init__(self, root_dir, inscription, num_classes, transform=None):

        self.root_dir = root_dir
        self.transform = transform
        self.inscription = inscription

        self.data_path = os.path.join(root_dir, inscription,
                                      'stylegan_gen_class_' + inscription)

        self.ticks = [os.path.join(self.data_path, f) for f in os.listdir(self.data_path)
            if not os.path.isfile(os.path.join(self.data_path, f))]
        self.ticks.sort()
        sg = 0.9 # Maximum quality score for the generated image
        self.thresholds = np.array([sg * (i/num_classes) for i in range(1,
            num_classes + 1)])

        self.img_paths = []
        self.labels = []
        max_len = len(self.ticks) + 1
        for idx, tick in enumerate(self.ticks):
            imgs = [os.path.join(tick, f) for f in os.listdir(tick)
                if os.path.isfile(os.path.join(tick, f))]
            self.img_paths.extend(imgs)
            score = sg*((idx+1)/max_len)
            mbc_answer = list((self.thresholds > score).astype(int))
            self.labels.extend(itertools.repeat(mbc_answer, len(imgs)))

        self.labels = torch.tensor(self.labels)

    def __getitem__(self, idx):
        image = read_image(self.img_paths[idx])

        if self.transform:
            image = self.transform(image)

        # Scale images from [0,255] to [0.0, 1.0]
        image = image / 255

        item_dict = {
            'image': image,
            'label': self.labels[idx],
            'path': self.img_paths[idx]
        }

        return item_dict

    def __len__(self):
        return len(self.img_paths)
