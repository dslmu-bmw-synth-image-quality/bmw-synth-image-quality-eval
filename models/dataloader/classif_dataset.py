import os
from torchvision.io import read_image

from models.dataloader.inscri_dataset import InscriptionDataset


class ClassificationDataset(InscriptionDataset):
    '''
        Dataset Class for Classification of Inscriptions
        : inscription: inscription type to be loaded
    '''
    def __init__(self, device, root_dir, inscription, inscri_to_class_map, flipped=False, real_images=False, transform=None):
        super(ClassificationDataset, self).__init__(device,
                                                    root_dir,
                                                    inscription,
                                                    flipped=flipped,
                                                    real_images=real_images,
                                                    transform=transform)

        self.label = inscri_to_class_map[inscription]

    def __getitem__(self, idx):
        image_path = os.path.join(self.data_path, self.img_paths_list[idx])
        image = read_image(image_path)

        if self.transform:
            image = self.transform(image)

        # Scale images from [0,255] to [0.0, 1.0]
        image = image / 255

        item_dict = {
            'image': image,
            'label': self.label
        }

        return item_dict
