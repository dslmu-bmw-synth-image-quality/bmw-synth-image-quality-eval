import os
from torch.utils.data import Dataset
from torchvision.io import read_image


class InscriptionDataset(Dataset):
    '''
        Base Dataset Class
        : inscription: inscription type to be loaded
    '''

    def __init__(self, device, root_dir, inscription, flipped=False, real_images=False, transform=None):
        self.device = device
        self.root_dir = root_dir
        self.transform = transform
        self.inscription = inscription
        self.flipped = flipped
        self.real_images = real_images
        # gives the correct folders in 'stylegan_gen_class_' to take images from
        self.folder_dict = {
            '40i': 'tick40_kimg4085',
            '320i': 'tick55_kimg5000',
            '330e': 'tick55_kimg5000',
            '730Li': 'tick55_kimg5000',
            '730Li_256': 'tick55_kimg5000',
            '740Li': 'tick55_kimg5000',
            '740Li_256': 'tick55_kimg5000'
        }

        if real_images:
            self.data_path = os.path.join(root_dir, inscription, 'model_inscription_' + inscription)
        elif self.flipped:
            if inscription == '40i':
                self.data_path = os.path.join(root_dir, inscription, 'stylegan_gen_class_' + inscription)
            else:
                self.data_path = os.path.join(root_dir, inscription, 'stylegan_gen_class_' + inscription,
                                              'tick50_kimg4705')
        else:
            self.data_path = os.path.join(root_dir, inscription, 'non_flipped', 'stylegan_gen_class_' + inscription,
                                          self.folder_dict[inscription])

        self.img_paths_list = [f for f in os.listdir(self.data_path)
                               if os.path.isfile(os.path.join(self.data_path, f))]

    def __getitem__(self, idx):
        image_path = os.path.join(self.data_path, self.img_paths_list[idx])
        image = read_image(image_path)

        if self.transform:
            image = self.transform(image)

        # Scale images from [0,255] to [0.0, 1.0]
        image = image / 255

        return image

    def __len__(self):
        return len(self.img_paths_list)
