import warnings
warnings.filterwarnings('ignore')

from models.utils.gmm_computation import *
from tqdm import tqdm


def brisque_scores(data_path, output_file,data_folder, filter=None, n_values=0, last=False, silent=False):
    """
    Computes the brisque_scores of the images in the passed folder.
    - data_path: folder with the different tick images. Included ticks are filtered with filter & n_values
    - output_file: where to save the .txt with scores and image location
    - data_folder: folder in which all images are saved, in order to generate system independent paths
    - filter: list of booleans of which images to include (takes precedence over other inputs)
    - n_values: returns n values. First n if last=False, otherwise last n values
    """
    # Hack required due to issues with the loading of the svm. Directory is reset after function had run
    predir = os.getcwd()
    currentdir = os.path.abspath(os.getcwd())
    parentdir = os.path.dirname(currentdir)
    brisquedir = os.path.join(parentdir, 'models', 'brisque', 'libsvm', 'python')
    os.chdir(brisquedir)
    from brisquequality import test_measure_BRISQUE as brisque

    directories = path_list(data_path, filter, n_values, last)

    if len(directories) == 0:
        paths = [data_path]
    else:
        paths = [os.path.join(data_path, dir) for dir in directories]

    if not silent:
        print("The following directories are used to compute scores:\n", directories)

    model = data_path.split("_")[-1]
    if len(model) > 5:
        model = data_path.split("\\")[-1]
        model = model.split("/")[-1]

    if output_file[-4:] != ".txt":
        output_name = f"{model}__" + "_".join([str(dir[:6]) for dir in directories]) + ".txt"
        output_file = os.path.join(output_file, output_name)

    files = []
    for path in paths:
        path = pathlib.Path(path)

        files += list(path.glob('*.jpg')) + list(path.glob('*.jpeg')) + list(path.glob('*.png'))

    with open(output_file, 'wt') as f:
        for image in tqdm(files):
            this_score = brisque(str(image))
            image_file = str(image)
            image_file = image_file.split(os.path.split(os.path.basename(data_folder))[1])[1][1:]
            f.write("score of " + image_file + " is:\n")
            f.write(str(this_score))
            f.write("\n")

    os.chdir(predir)