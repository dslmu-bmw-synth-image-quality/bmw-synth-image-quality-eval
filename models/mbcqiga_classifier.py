import torch
import torch.nn as nn
import pytorch_lightning as pl
import copy
import numpy as np
from math import sqrt
from torch.autograd import Variable

class MBClassifier(pl.LightningModule):
    def __init__(self, params):
        super().__init__()

        self.params = params

        self.conv = nn.Sequential(
            nn.Conv2d(in_channels=params['in_channels'],
                      out_channels=params['1x1conv_layers'][0],
                      kernel_size=1),
            nn.BatchNorm2d(params['1x1conv_layers'][0]),
            nn.LeakyReLU(0.2, inplace=True),
        )


        self.linear = nn.Sequential(
            nn.Linear(params['1x1conv_layers'][0], params['linear_layer']),
            nn.BatchNorm1d(params['linear_layer']),
            nn.Linear(params['linear_layer'], params['num_classes']),
            nn.ReLU(inplace=True),
            nn.Sigmoid()
        )

    def forward(self, batch):
        pred = self.conv(batch)
        pred = self.linear(pred.squeeze())
        return pred

class MBCGIQAClassifier(pl.LightningModule):
    def __init__(self, params, encoder):
        super().__init__()

        self.params = params
        self.encoder = encoder

        self.classifier = MBClassifier(params)

        self._initialize_weights()
        
        self.loss = nn.BCELoss()

    def forward(self, batch):
        features = self.encoder(batch)
        pred = self.classifier(features[-1])   # Select only the embedding
        return pred

    def configure_optimizers(self):
        model_params = list(self.classifier.parameters())
        optim = torch.optim.SGD(model_params,
                          lr=self.params['lr'],
                          momentum=self.params['momentum'],
                          weight_decay=self.params['weight_decay'])
        return optim

    def general_step(self, batch, batch_idx):
        pred_label_vector = self.forward(batch['image']).squeeze()
        true_label_vector = batch['label'].to(torch.float32)

        loss = self.loss(pred_label_vector, true_label_vector)

        return loss, pred_label_vector, true_label_vector

    def training_step(self, batch, batch_idx):
        loss, _, _ = self.general_step(batch, batch_idx)

        self.logger.experiment.add_scalar(f"Loss/Train", loss, self.current_epoch)
        self.log("loss", loss)

        return {'loss': loss}

    def validation_step(self, batch, batch_idx):
        loss, pred_labels, true_labels = self.general_step(batch, batch_idx)

        accuracy = (sum(pred_labels == true_labels) / len(true_labels)) * 100.0

        self.logger.experiment.add_scalar(f"Loss/Val", loss, self.current_epoch)
        for idx, acc in enumerate(accuracy):
            self.logger.experiment.add_scalar(f"Accuracy/Val for classifier {idx}", acc, self.current_epoch)
        self.log("val_loss", loss)

        result_dict = {'val_loss': loss, 'accuracy': accuracy}

        return result_dict

    def _initialize_weights(self):
        for m in self.classifier.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, sqrt(2. / n))
                if m.bias is not None:
                    m.bias.data.zero_()
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                n = m.weight.size(1)
                m.weight.data.normal_(0, 0.01)
                m.bias.data.zero_()


    def get_encoder(self):
        return self.model.get_encoder()

    def test_step(self, batch, batch_idx):
        return self.validation_step(batch, batch_idx)
