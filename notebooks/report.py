import os

import math
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib import rcParams


def chunker(seq, size):
    """
    returns chunks of size: size of seq
    """
    return (seq[pos:pos + size] for pos in range(0, len(seq), size))


def load_score(score_path, plot_feature='score', bins=6, divisor=10000, include_ticks=False):
    """
    Loads the scores of the given files, discretices them into bins, divides the score for readability and
    returns a numpy array with the bucketized data and bucket labels
    - score_path: from where to load the scores
    - plot_feature: which feature of the DF to plot, preset to score
    - divisor: what to divide by for readability
    - bins: how strongly to bing
    - include_ticks: whether to add another column indicating the tick-value of the given image
    """
    imgs, vals = [], []

    with open(score_path) as f:
        for f_path, val in chunker(f.readlines(), 2):
            imgs.append(f_path[9:-5])
            vals.append(float(val))

    vals = np.array(vals)
    vals = vals / divisor

    labels = list(range(0, bins))

    results = pd.DataFrame(vals, columns=[plot_feature])
    results.insert(0, 'file', imgs)
    results['score_bin'] = pd.qcut(results["score"], q=bins)
    results['score_bin_nr'] = pd.qcut(results["score"], q=bins, labels=labels)
    if include_ticks:
        results['tick'] = results.apply(lambda row: row.file.split("tick")[1][:2], axis=1)

    return results


def plot_overview(buckets, model_results, image_folder, plot_feature='score', image_path_list=False, heigth=20,
                  width=20, depth=6,
                  include_ticks=False, palette=None, hue_order=None, order="mixed"):
    """
    Plots the distribution of each bucket with depth-1 example images of the best scores per bucket.
    Height and width adapt the display size.
    - buckets: amount of buckets in data
    - model_results: DF for the given bucketed model, as created by load_score
    - image_folder: where to find the images
    - plot_feature: which feature of the DF to plot, preset to score
    - image_path_list: whether passed is a list of full path names, or simply the folder location
    - palette: the colour palette to use
    - heigth: the heigth of the plot
    - width: the width of the plot
    - depth: total depth of display
    - include_ticks: whether to display the ticks together with the score
    - palette: a specific palette to be used by sns for plotting
    - hue_order: the ordering of the given palette
    - order: whether the images should be "mixed" from top and bottom images or only "top" or "bottom"
    """
    plt.rcParams['figure.figsize'] = [heigth, width]
    fig, axs = plt.subplots(depth, len(buckets))
    fig.tight_layout()
    for i in buckets:
        bucket_slice = model_results[model_results.score_bin_nr == buckets[i]].sort_values(by=plot_feature,
                                                                                           ascending=False)
        if include_ticks:
            if palette:
                try:
                    sns.histplot(bucket_slice, x="score", bins=20, ax=axs[0, i], legend=True, hue='tick',
                                 palette=palette, hue_order=hue_order).set_title(
                        f'{i + 1}/{len(buckets)}th of scores',
                        fontstyle='normal', fontsize=18)
                except:
                    sns.histplot(bucket_slice, x="score", bins=20, ax=axs[0, i], legend=True, hue='tick',
                                 palette=palette).set_title(
                        f'{i + 1}/{len(buckets)}th of scores',
                        fontstyle='normal', fontsize=18)
            else:
                sns.histplot(bucket_slice, x="score", bins=20, ax=axs[0, i], legend=True, hue='tick').set_title(
                    f'{i + 1}/{len(buckets)}th of scores',
                    fontstyle='normal', fontsize=18)
        else:
            sns.histplot(bucket_slice, bins=20, ax=axs[0, i], legend=True).set_title(
                f'{i + 1}/{len(buckets)}th of scores',
                fontstyle='normal', fontsize=18)

        mid = math.ceil((depth - 1) / 2)

        for j in range(1, depth):
            if order == "mixed":
                if j <= mid:
                    if image_path_list:
                        image_path = image_folder[j - 1]
                    else:
                        image_path = os.path.join(image_folder, bucket_slice['file'].iloc[j - 1])
                        score_value = round(bucket_slice[plot_feature].iloc[j - 1], 1)
                else:
                    if image_path_list:
                        image_path = image_folder[mid - j]
                    else:
                        image_path = os.path.join(image_folder, bucket_slice['file'].iloc[mid - j])
                        score_value = round(bucket_slice[plot_feature].iloc[mid - j], 1)
            elif order == "top":
                if image_path_list:
                    image_path = image_folder[j - 1]
                else:
                    image_path = os.path.join(image_folder, bucket_slice['file'].iloc[j - 1])
                    score_value = round(bucket_slice[plot_feature].iloc[j - 1], 1)
            elif order == "bottom":
                if image_path_list:
                    image_path = image_folder[-j]
                else:
                    image_path = os.path.join(image_folder, bucket_slice['file'].iloc[-j])
                    score_value = round(bucket_slice[plot_feature].iloc[-j], 1)

            image = plt.imread(image_path)
            axs[j, i].imshow(image)
            axs[j, i].axis('off')
            if include_ticks:
                tick = bucket_slice['tick'].iloc[j - 1]
                axs[j, i].set_title(f'Score: {score_value}; Tick: {tick}', fontstyle="italic")
            else:
                axs[j, i].set_title(f'Score: {score_value}', fontstyle="italic")

    plt.show()


def plot_model_overview(all_models, width=22, plot_feature='score'):
    """
    Plots an overview for all models consisting of a histplot with the models,
    a histplot (KDE) of all image scores and a boxplot of the score distribution by model.
    - all_models: DF with the data for all models, as created by all_model_df
    - plot_feature: which feature of the DF to plot, preset to score
    - width: the width of the plot
    """
    rcParams['figure.figsize'] = [width, 8]
    fig, axs = plt.subplots(1, 3)
    fig.tight_layout()
    _ = sns.histplot(all_models, x=plot_feature, stat="density", common_norm=False, hue="model", ax=axs[0]).set(
        title="Distribution of overall values")
    _ = sns.kdeplot(x=all_models[plot_feature], hue=all_models.model, ax=axs[1]).set(
        title="Distribution of overall values (KDE)")
    _ = sns.boxplot(x=all_models.model, y=all_models[plot_feature], ax=axs[2]).set(
        title="Distribution of overall values (Boxplot)")
    plt.show()


def plot_individual_model_overview(model_results, all_models, model, palette=None,  tick=True,
                                   width=22, plot_feature='score', xlim=None):
    """
    Plots an overview for an individual model consisting of a histplot with ticks for this model,
    a histplot of all image scores and a boxplot of the score distribution.
    - model_results: DF for the given bucketed model, as created by load_score
    - all_models: DF with the data for all models, as created by all_model_df
    - model: model being plotted
    - plot_feature: which feature of the DF to plot, preset to score
    - palette: the colour palette to use
    - width: the width of the plot
    """
    rcParams['figure.figsize'] = [width, 8]
    if tick:
        fig, axs = plt.subplots(1, 3)
        fig.tight_layout()
        if palette:
            _ = sns.histplot(model_results, x=plot_feature, bins=100, ax=axs[0], hue='tick', palette=palette).set(
                title=f"Distribution of image scores for {model}")
            _ = sns.histplot(all_models, x=plot_feature, bins=100, ax=axs[1]).set(
                title=f"Distribution of all image scores")
            _ = sns.boxplot(x=model_results.tick, y=model_results[plot_feature], palette=palette).set(
                title="Score distribution by ticks")
        else:
            _ = sns.histplot(model_results, x=plot_feature, bins=100, ax=axs[0], hue='tick').set(
                title=f"Distribution of image scores for {model}")
            _ = sns.histplot(all_models, x=plot_feature, bins=100, ax=axs[1]).set(
                title=f"Distribution of all image scores")
            _ = sns.boxplot(x=model_results.tick, y=model_results[plot_feature]).set(title="Score distribution by ticks")
    else:

        fig, axs = plt.subplots(1, 2)
        fig.tight_layout()
        _ = sns.histplot(model_results, x=plot_feature, bins=100, binrange=xlim, ax=axs[0], ).set(
            title=f"Distribution of image scores for {model}", xlim=xlim)
        if palette:
            trunc_model = all_models.copy()
            trunc_model['model'] = trunc_model["model"].apply(lambda x: "other" if x != model else model)
            _ = sns.histplot(trunc_model, x=plot_feature, bins=100, binrange=xlim, common_norm=False, stat='density',
                             hue="model", palette=palette, hue_order=[model, "other"], ax=axs[1]).set(
                title=f"Distribution of all image scores", xlim=xlim)
        else:
            _ = sns.histplot(all_models, x=plot_feature, bins=100, binrange=xlim, ax=axs[1]).set(
                title=f"Distribution of all image scores", xlim=xlim)

    plt.show()


def plot_score_amount_overview(score_comparison: pd.DataFrame, results: pd.DataFrame, plot_feature='score', width=22,
                               sizes=(400, 1200),
                               xlim=None):
    """
    Plots an overview of the distributions of all scores.
    - score_comparison: DF with amount, mean, median, variance and model columns
    - results: overall result dataframe
    - plot_feature: which feature of the DF to plot, preset to score
    - width: width paramater
    - sizes & xlim: paramaters for sns (scatterplot & histplot)
    """

    hue_order_real = score_comparison.model.to_list()

    rcParams['figure.figsize'] = [width, 8]
    fig, axs = plt.subplots(1, 3)
    fig.tight_layout()

    _ = sns.scatterplot(data=score_comparison, x='amount', y='mean', hue='model', size='variance', sizes=sizes,
                        ax=axs[0]).set(title="Relationship between mean, variance and training amount")
    _ = sns.kdeplot(x=results[plot_feature], hue=results.model, hue_order=hue_order_real, ax=axs[1]).set(
        title="Distribution of overall values (KDE)", xlim=xlim)
    _ = sns.histplot(x=results[plot_feature], bins=40, binrange=xlim, hue=results.model, hue_order=hue_order_real,
                     ax=axs[2]).set(
        title="Distribution of overall values", xlim=xlim)
    plt.show()


def all_model_df(models, gmm_dir, divisor=10000, bins=1, file_ending=None, include_ticks=False):
    """
    Returns a df which adds the scores of all models passed in a list and returns a df with file_path, score and model.
    - models: list of models
    - gmm_dir: where score file is stored
    - divisor: what to divide by for readability
    - bins: how strongly to bin
    - file_ending: string which defines what comes after the model number in the score file (without file ending).
        To specify if files different from the standard naming are used.
    - include_ticks: whether to add another column indicating the tick-value of the given image
    """
    all_models = pd.DataFrame()
    for model in models:
        # load the scores, add the model type and drop the bins
        if file_ending:
            scores_path = os.path.join(gmm_dir, f'{model}{file_ending}.txt')
        else:
            scores_path = os.path.join(gmm_dir, f'{model}tick50.txt')

        model_results = load_score(scores_path, bins=bins, divisor=divisor)
        model_results['model'] = model
        if include_ticks:
            model_results['tick'] = model_results.apply(lambda row: row.file.split("tick")[1][:2], axis=1)
        model_results.drop(['score_bin', 'score_bin_nr'], axis=1, inplace=True)

        # add initial model scores to overall ones
        all_models = all_models.append(model_results)

    all_models = all_models.reset_index(drop=True)

    return all_models


def create_model_overview(all_models, plot_feature='score'):
    """
    Returns a df with relevant statistical characteristics of the passed models.
    Requires a df with score and model column, such as one returned by all_model_df
    - plot_feature: which feature of the DF to plot, preset to score
    """
    Index = []
    Min = []
    Mean = []
    Median = []
    Max = []
    Samples = []

    for model in all_models.model.unique():
        Index.append(model)
        selection = all_models[all_models['model'] == model]
        Min.append(selection[plot_feature].min())
        Max.append(selection[plot_feature].max())
        Mean.append(selection[plot_feature].mean())
        Median.append(selection[plot_feature].median())
        Samples.append(len(selection))

    stat_overview = pd.DataFrame({"Min": Min, "Mean": Mean, "Median": Median, "Max": Max, "Samples": Samples},
                                 index=Index)

    return stat_overview
