#!/usr/bin/python

import argparse
import os, random

n = 100
models = ['320i','330e', '730Li', '740Li']
out_file = "random_samples.txt"

def samp(models: list, n: int):
	'''returns n randomly sampled images for models given'''
	res = []
	for model in models:
		imgs = random.sample([os.path.join(dp, f) for dp, n, fn in os.walk(f"../data/{model}/stylegan_gen_class_{model}") for f in fn], n)  
		res.extend(imgs)

	return res

def main():
	samples = samp(models, n)
	with open(out_file, "w") as f:
		for l in samples:
			f.write("%s\n" % l)

if __name__ == "__main__":
	main()
